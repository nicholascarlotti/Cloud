# filesystem_manager.py    --    routes received files to the correct locations
import os
import os.path
from core.filesystem.fs_models import File, Directory
from shutil import rmtree

ROOT_FS_PATH = "corefs/"


def get_user_file_tree(username) -> Directory:
    """
    Returns a Directory object containing the whole
    hierarchy of the user's folder. 
    """
    user_root = get_user_path(username)
    result = get_directory_tree(user_root)
    return result


def get_user_file_hirierarchy_json(username) -> str:
    user_root_dir = get_user_file_tree(username)
    # Need to set the name of the root directory to
    # something harmless or we would be leaking info.
    user_root_dir.set_name(username)
    return user_root_dir.to_json()


def get_directory_tree(directory_path) -> Directory:
    """
    Returns a Directory object containing all the hierarchy
    from the entered folder down the end.
    """

    # Trim the string to remove the hirierachy and the leading
    # and trailing slashes.
    trimmed_directory_path = directory_path.replace("corefs", "")
    last_slash_index = trimmed_directory_path[:-1].rfind("/")
    trimmed_directory_path = trimmed_directory_path[last_slash_index + 1:-1]

    result = Directory(trimmed_directory_path)
    files = os.listdir(directory_path)
    for file in files:
        if(os.path.isdir(directory_path + file)):
            result.add_file(get_directory_tree(directory_path + file + '/'))
        else:
            stats = os.stat(directory_path + file)
            file_size = stats.st_size
            file_date = stats.st_mtime
            result.add_file(File(file, file_size, file_date))
    return result


def get_directory_content(username, directory="") -> Directory:
    """
    Returns a Directory object containing all the files
    in the requested directory.
    """
    result = Directory(directory)
    if not (directory[-1:] == '/'):
        directory += '/'
    absolute_path = get_user_path(username) + directory
    print(os.path.join(os.getcwd(), absolute_path))
    files = os.listdir(absolute_path)
    for file in files:
        if(os.path.isdir(absolute_path + file)):
            result.add_file(Directory(file))
        else:
            file_stats = os.stat(absolute_path + file)
            file_size = file_stats.st_mtime
            file_date = file_stats.st_size
            result.add_file(File(name=file, size=file_size, date=file_date))
    return result


def calculate_parent_directory(directory) -> str:
    """
    Helper function that returns a slice of 
    the directory string parameter 
    which represents the name of the parent directory.
    """
    if directory[-1] == '/':
        tmp = directory[:-1]

    tmp = tmp.rfind('/')
    tmp = tmp[:tmp]
    return tmp


def get_parent_directory(directory) -> str:
    """
    Returns a string representing the upper folder of the directory uri
    specified.
    Raises an exception if no upper folder is found.
    """
    upper = calculate_parent_directory(directory)
    if upper == '':
        raise Exception('No avilabile upper folder')
    return upper


def get_user_path(username) -> str:
    """
    Returns the path (relative to the ROOT_FS_PATH)
    to the root folder of a user.
    """
    return ROOT_FS_PATH + username + '/'


def add_userfile(file_object, username):
    """
    Writes a file to a path into the user's folder.
    TODO: Fix this function - there is no username/path validity
    checking.
    There also is the need for a third parameter "path", a relative path
    into the user's folder.
    write_file() does not exist. This code is incosistent.
    """
    path = get_user_path(username)
    write_file(file_object, username)


def add_folder(username, folder_path):
    path = get_user_path(username)
    path += folder_path
    os.mkdir(path)


def make_user_directory(username):
    os.mkdir(ROOT_FS_PATH + username)


def remove_file(username, file_path):
    complete_file_path = get_user_path(username) + file_path
    os.remove(complete_file_path)


def remove_folder(username, folder_path):
    complete_folder_path = get_user_path(username) + folder_path
    rmtree(complete_folder_path)


def path_exists(folderpath):
    return os.path.isdir(folderpath)


def file_exists(filepath):
    return os.path.isfile(filepath)


def get_file_content(username, file_path):
    """
    Returns the content of a file in string form.
    """
    complete_file_path = ROOT_FS_PATH + username + file_path

    if not file_exists(complete_file_path):
        print('{} {} {}'.format(ROOT_FS_PATH, username, file_path))
        raise Exception('Invalid filename {}'.format(complete_file_path))

    file = open(complete_file_path, 'r')
    file_name = os.path.basename(complete_file_path)
    try:
        file_content = file.read()
        is_binary = False
    except UnicodeDecodeError:
        file.close()
        file = open(complete_file_path, 'rb')
        file_content = file.read()
        is_binary = True

    return (file_content, file_name, is_binary)
