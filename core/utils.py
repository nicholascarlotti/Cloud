import base64

def bytes_to_base64(bytes):
    return base64.b64encode(bytes)

def base64_to_bytes(b64):
    return base64.b64decode(b64)

def string_to_base64(string):
    str_to_bytes = bytearray(string)
    return bytes_to_base64(str_to_bytes)

def base64_to_string(b64):
    b64_to_bytes = base64_to_bytes(b64)
    return b64_to_bytes.decode('utf-8')

def is_binary(tester):
    return type(tester) is bytes