//			Initialization
$('.menu_entry').hover(function(){
	$(this).addClass("menu_item_hover");
},
function(){
	$(this).removeClass("menu_item_hover");
});
$(".context_menu").addClass("no_contextmenu");
$('.no_contextmenu').on("contextmenu", function(event){
	event.preventDefault();
	event.stopPropagation();
});


var contextmenu = {

	selectedItemName : '',
	selectedItem : null,

	showUserDropDownMenu : function(){
		$('#user_dropdown').css('display', 'block');
	},


	hideFileContextMenu : function(){
		selectedItemName = "";
		$('#fileMenu').css('display', 'none');
	},

	hideFolderContextMenu : function(){
		selectedItemName = '';
		$('#folderMenu').css('display', 'none');
	},

	hideUserDropDownMenu : function(){
		selectedItemName = '';
		$('#user_dropdown').css('display', 'none');
	},

	hideAllContextMenus : function(){
		contextmenu.hideFileContextMenu();
		contextmenu.hideFolderContextMenu();
		contextmenu.hideUserDropDownMenu();
	},

	folderContextMenu : function(event, folderObject){
		//Prevent the default behaviour for
		//the right click
		event.stopPropagation();
		event.preventDefault();

		contextmenu.hideAllContextMenus();

		selectedItem = folderObject;
		selectedItemName = folderObject.getName();

		var x = event.clientX
		var y = event.clientY;
		
		//This is necessary when the user
		//tries to open a context menu when the page
		//has been scrolled even by a unit.
		//If we don't calculate the scroll offset
		//the menu will be spawned at a lower y coordinate
		//than the one it should be at.
		var scrollY = $('body').scrollTop();
		
		$('#folderMenu').css('position', 'absolute');
		$('#folderMenu').css('display', 'inline');
		$('#folderMenu').offset({top: y + scrollY, left: x});
	},


	fileContextMenu : function(event, fileName){
		event.stopPropagation();
		event.preventDefault();
	
		contextmenu.hideAllContextMenus();
		selectedItemName = fileName;
	
		var x = event.clientX;
		var y = event.clientY;

		//This is necessary when the user
		//tries to open a context menu and the page
		//has been scrolled even by a unit.
		//If we don't calculate the scroll offset
		//the menu will be spawned at a lower y coordinate
		//than the one it should be at.
		var scrollY = $('body').scrollTop();
		$('#fileMenu').css('position', 'absolute');
		$('#fileMenu').css('display', 'inline');
		$('#fileMenu').offset({top: y + scrollY, left: x});
	},


	downloadItem : function(event){
		// Make a frame present in the page navigate to 
		// the download url of a file.
		var targetFile = currentFolderPath + selectedItemName;
		$('#downloadFrame')[0].src = "/download?file=" + targetFile;
	},

	deleteFile : function(event){
		var form = new FormData();
		form.append("file", currentFolderPath + selectedItemName);
		console.log("Sending delete request for: " + currentFolderPath + selectedItemName);
		var settings = {
		"async": true,
		"crossDomain": true,
		"url": "http://127.0.0.1:5000/remove/file/",
		"method": "POST",
		"processData": false,
		"contentType": false,
		"mimeType": "multipart/form-data",
		"data": form,
		}

		$.ajax(settings).done(function (response) {
			console.log("Remove file response: " + response);
		});

		location.reload();
	},

	deleteFolder : function(event){
		if(selectedItem.constructor != Folder){
			return;
		}

		if(!selectedItem.isEmpty()){
			result = confirm("The folder you want to delete is not empty, are you sure you want to \
				delete its content?");
			if(result){
				//Delete folder
				api.deleteFolder(selectedItem.getFolderPath());
			}
		}else{
			//Delete folder
			api.deleteFolder(selectedItem.getFolderPath());
		}
	},

};
