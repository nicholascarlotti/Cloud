var breadcrumb = {

	 button_urls : [],
	breadcrumb_folders : [],

	/**
	*	@param {Folder} folder - The hirierarchy to build the crumbs from.	
	*/
	new_build_breadcrumb : function(folder){
		/*
			Will build a breadcrumb from the 
			hirierarchy given as parameter.
		*/
		var hirierarchy = [];
		do{
			hirierarchy.push(folder);
			folder = folder.getParentFolder();
		}while(folder);
		$("#breadcrumb").empty();

		for(var i = hirierarchy.length - 1; i >= 0; --i){
			var folderName = hirierarchy[i].getName();
			var $btn = $("<canvas class=\"breadcrumb_btn\">" + folderName + "</canvas>");
			$btn.hover(function(){
				var context = this.getContext("2d");
				context.fillStyle = "#C2C3CC";
			},
			function(){
				var context = this.getContext('2d');
				context.fillStyle = '#74888F';
			}
			);

			$btn.prop("folderObject", hirierarchy[i]);
			$btn.on("click", function(){
				setCurrentFolder(this.folderObject);
			});

			breadcrumb.draw_jq_canvas($btn, i != hirierarchy.length - 1, folderName);
			
			$("#breadcrumb").append($btn);

		}	

	},


	/**
	*	@deprecated
	*/
	build_breadcrumb : function(folder){breadcrumb.new_build_breadcrumb(folder);},


	draw_jq_canvas : function($canvas, has_trailing, canvas_text){
		var canvas = $canvas[0];
		var context = canvas.getContext('2d');
		canvas.height = 30;
		canvas.width = 90;
		var recomended_width = breadcrumb.measureTextWidth(canvas_text, 18); 
		console.log(recomended_width);
		if(recomended_width > 90){
			canvas.width = recomended_width;
		}else{
			canvas.width = 90;
		}

		context.beginPath();
	  	context.moveTo(0, 0);
	  	context.lineTo(canvas.width - 10, 0);
	  	context.lineTo(canvas.width, 15);
	  	context.lineTo(canvas.width - 10, 30);
	  	context.lineTo(0, 30);

	  	if(has_trailing){
	  		context.lineTo(10, 15);
	  	}
	  	context.closePath();
	  	context.lineWidth = 2;
	  	context.fillStyle = '#769ead';
	  	context.fill();
		context.font = "18px Verdana";

		//Modify these paramteres to relocate
		//text position inside of the breadcrumbs
	   	if(!has_trailing){
			context.strokeText(canvas_text, 10, 20);
	   	}else{
			context.strokeText(canvas_text, 15, 20);
	   	}


	},


	//Using this function to measure the width of a
	//canvas since i had some troubles using the 
	//standard method "measureText()".
	//The problem is that there are inconsistencies
	//when measuring before resizing the canvas and writing
	//on it after.
	measureTextWidth : function(text, font_size){
		return text.length * font_size / 1.4;
	},
}