from core.webinterface import app, login_manager, env
from flask import Flask, request, redirect, url_for, \
    render_template, make_response, g, send_from_directory, session
import core.filesystem.filesystem_manager as filesystem_manager
import core.users.user_management as user_management
from flask.ext.login import login_required, login_user, logout_user
from werkzeug.utils import secure_filename
from core.webapp.utils import ERROR_MESSAGES


@app.route('/index')
def index():
    return 'This is the index'


@app.route('/upload', methods=['GET', 'POST'])
@login_required
def upload_file():
    if request.method == 'POST':

        upload_folder = request.form['current_folder']
        file = request.files['file']
        print(upload_folder)
        if file:

            username = session['username']

        try:

            user = user_management.search_user(username)
            user.reload_folder_structure()

        except Exception:
            # TODO: Return the correct HTTP error code
            g.error_message = ERROR_MESSAGES['USER_NOT_FOUND']
            return error_page()

        filename = file.filename

        filepath = filesystem_manager.get_user_path(user.get_name())\
            + normalize_request_url(upload_folder) \
            + "/" + secure_filename(filename)

        print("[***DEBUG***]" + filepath)

        file.save(filepath)

        return 'The file was uploaded'

    template = env.get_template('upload.html')

    return template.render()


@app.route('/remove/file/', methods=['POST'])
@login_required
def remove_file():
    username = session['username']
    try:
        user_management.search_user(username)
    except Exception:
        # TODO: Return HTTP error code
        return "invalid username"
    file = request.form["file"]

    # This is an addictional check (not useful)
    # but since the file urls from the browser
    # come with the root folder at the start
    # we have to trim it while checking the
    # sanity of the request.
    print("Url pre-trim {}".format(file))
    if file[0:len(username) + 1] == username + "/":
        file = file[len(username) + 1:]
    print("Url post-trim {}".format(file))

    try:
        filesystem_manager.remove_file(username, file)
    except Exception:
        # TODO: This requires a different handling, maybe in the front end
        return "cannot remove a directory in this way"
    return "file deleted"


@app.route('/remove/folder/', methods=['POST'])
@login_required
def remove_directory():
    print(request.cookies.get("csrftoken"))
    username = session['username']
    try:
        user_management.search_user(username)
    except Exception:
        # TODO: Return an HTTP error code
        return "invalid username"
    directory = request.form['directory']

    # This is an addictional check (not useful)
    # but since the file urls from the browser
    # come with the root folder at the start
    # we have to trim it while checking the
    # sanity of the request.
    if directory[0:len(username) + 1] == username + "/":
        directory = directory[len(username) + 1:]

    filesystem_manager.remove_folder(username, directory)
    return "OK"


@app.route('/home', methods=['GET', 'POST'])
@login_required
def view_files():
    """
    TODO: Rework the home view to return metadata relative to all
    the files in a user's directory as json*.
    It will be client's duty to update itself on changes.
    * Actually, since the template system seems a viable and simpler
    choice it is possible i'm going to use it instead.

    GET - Return the template
    POST - Return json data for the user's directory
    """
    if request.method == 'GET':
        if not ('username' in session):
            return redirect(url_for('login'))
        else:
            username = session['username']
        try:
            user_management.search_user(username)
        except Exception:
            # TODO: Redirect to the error page
            g.error_message = ERROR_MESSAGES['USER_NOT_FOUND']
            return error_page()
        requested_folder = request.args.get('folder')
        requested_folder = normalize_request_url(requested_folder)

        # # TODO: Handle a possible error where the user's folder doesn't exist
        # # while the user does
        # directory_content = filesystem_manager.get_directory_content(
        #     username, requested_folder)

        # if directory_content.get_name() == "/":
        #     folder_tree = "/"
        # elif directory_content.get_name()[:-1] == "/":
        #     folder_tree = directory_content.get_name()
        # else:
        #     folder_tree = directory_content.get_name() + "/"
        # print(folder_tree)

        # tmp = build_folder_result(directory_content)
        # files_result = tmp['f']
        # folders_result = tmp['d']

        # Get the json metadata from the file system manager
        # and send it to the client
        user_file_hierarchy = filesystem_manager.get_user_file_hirierarchy_json(
            username)
        template = env.get_template('home.html')
        return template.render(files=user_file_hierarchy,
                               username=username)


@app.route('/registration', methods=['GET', 'POST'])
def register():
    if request.method == 'GET':
        template = env.get_template('registration.html')
        return template.render()
    elif request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        print("Got a registration request! ")
        print(username)

        # WARNING: This could fail if the username/password is longer
        # than expected from the database
        try:
            new_user = user_management.add_user(username, password)
        except Exception:
            return 'Error', 400
        g.logged_user = user_management.search_user(username).get_name()
        login_user(new_user)

        session['username'] = username

# TEMPORARY SOLUTION TO SERVE STATIC FILES


@app.route('/static/<file>')
def serve_static(file):
    return send_from_directory('core/webapp/static/', file)


@app.route('/static/fonts/<file>')
def serve_static_font(file):
    return send_from_directory('core/webapp/static/fonts', file)


@app.route('/static/images/<file>')
def serve_static_image(file):
    return send_from_directory('core/webapp/static/images', file)


@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'GET':
        # Check if the user is already logged in, if so
        # redirect him to the home.
        if('username' in session):
            return redirect(url_for('view_files'))
        # In the case of a simple GET request show the user
        # the login form
        template = env.get_template('login.html')
        return template.render()
    elif request.method == 'POST':

        username = request.form['username']
        password = request.form['password']
        try:
            # Validate the credentials
            user = user_management.check_login(username, password)
            g.logged_user = user.get_name()
            login_user(user)
            session['username'] = username

            # This function with this decorator will be
            # called by call_after_request_callback()
            # after the request has been processed in this method
            """
            @after_this_request
            def add_login_cookie(request):
                if hasattr(g, 'logged_user'):
                    expiration = datetime.datetime.now() \
                    + datetime.timedelta(days=1)
                    print('Setting cookie {}'.format(g.logged_user))
                    request.set_cookie('username', value = g.logged_user)
                    return request
                return redirect(url_for('view_files'))
            """

        except Exception as e:
            print(e)
            # WARNING: This could just return an error code
            # so that the frontend would just display a message
            return 'Invalid login'
        return redirect(url_for('view_files'))


@app.route('/logout')
@login_required
def logout():
    logout_user()
    if 'username' in session:
        session.pop('username')

    return redirect(url_for('login'))


@app.route('/download')
@login_required
def download():
    to_download = request.args.get('file')
    username = session['username']
    # TODO: check that the name appears in the database
    if not username:
        print('[ERROR] The username cookie was not set')

    file_content, file_name, is_binary = filesystem_manager.get_file_content(
        "", to_download)

    response = make_response(file_content)

    if is_binary:
        response.headers['Content-Type'] = 'application/octet-stream'

    response.headers[
        'Content-Disposition'] = "attachment; filename={}".format(file_name)
    return response


# @app.route('/error')
def error_page():
    error_message = g.error_message
    template = env.get_template('error_page.html')
    return template.render(error_message=error_message)


"""
    Helper functions
"""

# This method gets targeted by the decorator
# @after_this_request


def after_this_request(callback):
    if not hasattr(g, 'after_request_callbacks'):
        g.after_request_callbacks = []
    g.after_request_callbacks.append(callback)
    return callback

# This decorator indicates that this
# function has to be called whenever a request
# has finished getting processed


@app.after_request
def call_after_request_callbacks(response):
    for callback in getattr(g, 'after_request_callbacks', ()):
        callback(response)
    return response


@login_manager.user_loader
def user_loader(username):
    try:
        user = user_management.search_user(username)
        print('Loaded user {}'.format(user))
    except Exception:
        user = None
    return user


def build_folder_result(folder):
    result = {'d': [], 'f': [], }
    for children in folder.get_files():
        if(isinstance(children, filesystem_manager.Directory)):
            result['d'].append(children.get_name())
        else:
            result['f'].append({
                "name": children.get_name(),
                "size": children.get_size(),
                "date": children.get_last_modified(),
            })
    return result


def normalize_request_url(url):
    result = url
    if not url:
        result = '/'
    if '..' in result:
        raise InvalidUrlException("Invalid url {}".format(url))
    return result


class InvalidUrlException(Exception):

    def __init__(self, message):
        super(Exception, message)
