ERROR_MESSAGES = {
    'USER_NOT_FOUND' : 'The inserted username is not valid or doesn\'t exist',
    'INVALID_REQUEST' : 'The submitted request was not valid',
}
